<?php

declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\DTO\PeselDTO;
use AppBundle\Form\PeselType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * HomeController class
 */
class HomeController extends Controller
{
    private SessionInterface $session;

    /**
     * @Route("/", name="app_home")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->session = $request->getSession();

        $peselDTO = $this->createPeselDTO();

        $peselForm = $this->createForm(PeselType::class, $peselDTO);
        $peselForm->handleRequest($request);

        if ($peselForm->isSubmitted() && $peselForm->isValid()) {
            $this->addFlash('success', 'PESEL number is correct !');
            $this->savePeselInSession($peselDTO);

            return $this->redirectToRoute('app_home');
        }

        return $this->render('default/controller/app_home.html.twig', [
            'title' => 'Tomasz Bukowski - PESEL Validation',
            'pesel_form' => $peselForm->createView()
        ]);
    }

    /**
     * @return PeselDTO
     */
    private function createPeselDTO(): PeselDTO
    {
        $peselDTO = new PeselDTO();

        $serializedPesel = $this->session->get('pesel_dto');
        if (!is_null($serializedPesel)) {
            $peselDTO->unserialize($serializedPesel);
        }

        return $peselDTO;
    }

    /**
     * @param PeselDTO $peselDTO
     * @return void
     */
    private function savePeselInSession(PeselDTO $peselDTO): void
    {
        $this->session->set('pesel_dto', $peselDTO->serialize());
    }
}
