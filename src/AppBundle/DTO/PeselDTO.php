<?php

declare(strict_types=1);

namespace AppBundle\DTO;


/**
 * PeselDTO class
 */
class PeselDTO implements \Serializable
{
    private int $pesel;

    /**
     * PeselDTO constructor
     *
     * @param string $pesel
     */
    public function __construct(string $pesel = null)
    {
        if (ctype_digit($pesel)) {
            $this->pesel = (int) $pesel;
        }
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->pesel
        ]);
    }

    /**
     * @param string $serialized
     * @return void
     */
    public function unserialize($serialized): void
    {
        list(
            $this->pesel
        ) = unserialize($serialized);
    }

    /**
     * @return integer
     */
    public function getPesel(): int
    {
        return $this->pesel;
    }

    /**
     * @param string|null $pesel
     * @return void
     */
    public function setPesel(?string $pesel): void
    {
        if (ctype_digit($pesel)) {
            $this->pesel = (int) $pesel;
        }
    }
}
