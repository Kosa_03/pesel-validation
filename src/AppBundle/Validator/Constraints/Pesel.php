<?php

declare(strict_types=1);

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 * 
 * Pesel class
 */
class Pesel extends Constraint
{
    public $message = 'PESEL is not valid';
}
