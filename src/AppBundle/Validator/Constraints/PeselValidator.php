<?php 

declare(strict_types=1);

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * PeselValidator class
 */
class PeselValidator extends ConstraintValidator
{
    /**
     * @param mixed $pesel
     * @param Constraint $constraint
     * @return void
     */
    public function validate($pesel, Constraint $constraint)
    {
        if (is_null($pesel)) {
            return;
        }
        
        $peselLength = $this->checkPeselLength($pesel);
        if ($peselLength) {
            $peselControlSum = $this->checkControlSum($pesel);
            $peselDateCorrectness = $this->checkDateCorrectness($pesel);
        }

        if (!$peselLength || !$peselControlSum || !$peselDateCorrectness) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    /**
     * @param string $pesel
     * @return boolean
     */
    private function checkPeselLength(string $pesel): bool
    {
        if (mb_strlen($pesel) == 11) {
            return true;
        }

        return false;
    }

    /**
     * @param string $pesel
     * @return boolean
     */
    private function checkControlSum(string $pesel): bool
    {
        $weight = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
        $pesel = array_map('intval', mb_str_split($pesel));

        for ($controlSum = 0, $i = 0; $i < 10; $i++) {
            $result = $weight[$i] * $pesel[$i];
            
            if ($result > 9) {
                $controlSum += ($result % 10);
            } else {
                $controlSum += $result;
            }
        }

        if ($controlSum > 9) {
            $controlDigit = 10 - ($controlSum % 10);
        } else {
            $controlDigit = 10 - $controlSum;
        }

        if ($controlDigit == $pesel[10]) {
            return true;
        }

        return false;
    }

    /**
     * @param string $pesel
     * @return boolean
     */
    private function checkDateCorrectness(string $pesel): bool
    {
        $year = (int) mb_substr($pesel, 0, 2);
        $month = (int) mb_substr($pesel, 2, 2);
        $day = (int) mb_substr($pesel, 4, 2);

        if (($month >= 0) && ($month < 20)) {
            $year += 1900;
            $month -= 0;

        } elseif (($month >= 20) && ($month < 40)) {
            $year += 2000;
            $month -= 20;

        } elseif (($month >= 40) && ($month < 60)) {
            $year += 2100;
            $month -= 40;

        } elseif (($month >= 60) && ($month < 80)) {
            $year += 2200;
            $month -= 60;

        } elseif (($month >= 80) && ($month < 100)) {
            $year += 1800;
            $month -= 80;
        }

        if (!checkdate($month, $day, $year)) {
            return false;
        }

        $currentDateTime = strtotime('now');
        $peselDateTime = strtotime($day . '-' . $month . '-' . $year);

        if ($peselDateTime >= $currentDateTime) {
            return false;
        }

        return true;
    }
}
