<?php

namespace AppBundle\Form;

use AppBundle\Validator\Constraints\Pesel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * PeselType class
 */
class PeselType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pesel', TextType::class, [
                'label' => 'PESEL',
                'required' => false,
                'attr' => [
                    'maxlength' => 11
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'PESEL can not be blank'
                    ]),
                    new Length([
                        'min' => 11,
                        'max' => 11,
                        'exactMessage' => 'PESEL should have exactly {{ limit }} characters'
                    ]),
                    new Regex([
                        'pattern' => '/^\d+$/',
                        'message' => 'PESEL should be a number'
                    ]),
                    new Pesel([
                        'message' => 'PESEL is not valid'
                    ])
                ]
            ])
            ->add('validate', SubmitType::class, [
                'label' => 'Validate',
                'attr' => array(
                    'class' => 'btn-outline-primary'
                )
            ])
        ;
    }
}
