<?php

declare(strict_types=1);

namespace Tests\AppBundle\Validator\Constraints;

use AppBundle\Validator\Constraints\Pesel;
use AppBundle\Validator\Constraints\PeselValidator;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;


/**
 * PeselValidatorTest class
 */
class PeselValidatorTest extends ConstraintValidatorTestCase
{
    /**
     * @return ConstraintValidator
     */
    protected function createValidator(): ConstraintValidator
    {
        return new PeselValidator();
    }

    /**
     * @dataProvider getValidValues
     *
     * @param array $value
     * @return void
     */
    public function testValidValues($value): void
    {
        $this->validator->validate($value, new Pesel());

        $this->assertNoViolation();
    }

    /**
     * @return array
     */
    public function getValidValues(): array
    {
        return [
            ['67020445314'],    // 1967.02.04; sex: m;
            ['12280887707'],    // 2012.08.08; sex: f; 
            ['99122874404'],    // 1999.12.28; sex: f;
            ['00290104914'],    // 2000.09.01; sex: m;
            ['21213010507']     // 2021.01.30; sex: f;
        ];
    }

    /**
     * @dataProvider getInvalidValues
     *
     * @param array $value
     * @return void
     */
    public function testInvalidValues($value): void
    {
        $this->validator->validate($value, new Pesel());

        $this->buildViolation('PESEL is not valid')
            ->assertRaised();
    }

    /**
     * @return array
     */
    public function getInvalidValues(): array
    {
        return [
            ['1234'],           // Too short PESEL
            ['1234567890123'],  // Too long PESEL
            ['abc'],            // Not a number PESEL
            ['00290104910'],    // Bad control digit
            ['30452839501']     // Correct PESEL, but doesn't exists. Birth date 2130.05.28
        ];
    }
}
